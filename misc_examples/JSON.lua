#!/usr/bin/lua
-- Using the luajson library:
local json = require("json")
 
local json_data = [=[[
    42,
    3.14159,
    [ 2, 4, 8, 16, 32, 64, "apples", "bananas", "cherries" ],
    { "H": 1, "He": 2, "X": null, "Li": 3 },
    null,
    true,
    false
]]=]
 
print("Original JSON: " .. json_data)
local data = json.decode(json_data)
json.util.printValue(data, 'Lua')
print("JSON re-encoded: " .. json.encode(data))
 
local data = {
    42,
    3.14159,
    {
        2, 4, 8, 16, 32, 64,
        "apples",
        "bananas",
        "cherries"
    },
    {
        H = 1,
        He = 2,
        X = json.util.null(),
        Li = 3
    },
    json.util.null(),
    true,
    false
}
 
print("JSON from new Lua data: " .. json.encode(data))
-- Since in Lua nil signifies an undefined value, i.e. a variable or table entry with a nil value is undistinguishable from an undefined variable or non-existing table entry, a null value in JSON notation is decoded to a special function value, which ensures that it can be re-encoded properly to null again. To manually insert a null value in the JSON output, use the json.util.null function.
