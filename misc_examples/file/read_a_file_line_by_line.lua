#!/usr/bin/lua
filename = "input.txt"
fp = io.open( filename, "r" )
 
for line in fp:lines() do
    print( line )
end
 
fp:close()

--	Simpler version
-- The following achieves the same result as the example above, including implicitly closing the file at the end of the loop.

for line in io.lines("input.txt") do
  print(line)
end
